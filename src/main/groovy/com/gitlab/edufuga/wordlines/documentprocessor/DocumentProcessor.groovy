package com.gitlab.edufuga.wordlines.documentprocessor

import java.util.regex.Matcher
import java.util.regex.Pattern

class DocumentProcessor {
    private String fileName
    private File file
    private String fileWithoutExtension

    private Map<String, Set<String>> examples = Collections.synchronizedMap(new HashMap<>())
    private Set<String> words = new LinkedHashSet<>() // Order of the document.
    private List<String> wordsIncludingRepetitions = new ArrayList<>()
    private Map<String, Integer> frequencyOfWords = Collections.synchronizedMap(new TreeMap<>())

    private static Pattern SENTENCE = ~/»?‘?.*?[\.\?\!]”?’?«?/
    private static String WORDS_PATTERN = /[^-\.,:;…¡!¿?—\/–'"‘“”„’«»‹›()\[\]{}|_<>\\\^`+%*=~$&©@#⇒⇔Ã¼¾\d\s]*/

    DocumentProcessor(String fileName) {
        this.fileName = fileName
        assert fileName.endsWith(".txt")
        this.fileWithoutExtension = fileName-".txt"
        this.file = new File(fileName)
    }

    void process() {
        Set<String> sentences = extractSentences()

        extractWordsAndExampleSentences(sentences)

        writeUniqueWordsInFrequencyOrder()

        writeWordsInBookOrderIncludingRepetitions()

        writeUniqueWordsInAlphabeticalOrder()

        writeSentences(sentences)

        createSentenceFiles()

        writeWordFrequencies()
    }

    private Set<String> extractSentences() {
        println "Processing the sentences within the document '$fileName'."

        List<String> lines = file.readLines().grep { it }
        println "Number of lines: ${lines.size()}"

        Set<String> sentences = new LinkedHashSet<>()
        lines.forEach { line ->
            Matcher matcher = line =~ SENTENCE
            while (matcher.find()) {
                sentences.add(matcher.group().trim())
            }
        }

        println "Number of sentences: ${sentences.size()}"

        return sentences
    }

    private void extractWordsAndExampleSentences(Set<String> sentences) {
        println "Extracting words and example sentences."

        // Word grouping including all the sentences containing that word.
        sentences.each { sentence ->
            def words = sentence =~ WORDS_PATTERN
            words.grep().each {
                this.words.add(it)
                wordsIncludingRepetitions.add(it)
                examples.get(it as String, new HashSet<String>()).add(sentence)
            }
        }

        println "Number of unique words: "+ words.size()
        println "Number of words (repetitions included): " + wordsIncludingRepetitions.size()
    }

    private void writeUniqueWordsInFrequencyOrder() {
        File wordsFileBookOrder = new File(fileWithoutExtension + "_words_in_order.txt")
        wordsFileBookOrder.text = "" // empty file
        words.each { wordsFileBookOrder << it + "\n" }
    }

    private void writeWordsInBookOrderIncludingRepetitions() {
        File wordsIncludingRepetitionsFile = new File(fileWithoutExtension + "_splitted.txt")
        wordsIncludingRepetitionsFile.text = "" // empty file
        wordsIncludingRepetitions.each { wordsIncludingRepetitionsFile << it + "\n" }
    }

    private void writeUniqueWordsInAlphabeticalOrder() {
        def wordsFile = new File(fileWithoutExtension + "_words.txt")
        wordsFile.text = "" // empty file
        words.sort().each { wordsFile << it + "\n" }
    }

    private void writeSentences(Set<String> sentences) {
        def sentencesFile = new File(fileWithoutExtension + "_sentences.txt")
        sentencesFile.text = "" // empty file
        sentences.sort().each { sentencesFile << it + "\n" }
    }

    private void createSentenceFiles() {
        // Create a directory for every book with files for each word inside the book/document.
        // The content of the files for each word are the sentences that contain that word.
        // The information book -> words -> sentences is saved.
        def examplesDirectoryForDocument = new File(fileWithoutExtension)
        examplesDirectoryForDocument.deleteDir() // delete previous directory contents
        examplesDirectoryForDocument.mkdirs()

        examples.each { String word, Set<String> sentencesForWord ->
            def wordFile = new File(fileWithoutExtension + "/" + word + ".txt")
            wordFile.text = "" // empty file contents if exists
            wordFile << sentencesForWord.join("\n")

            frequencyOfWords.put(word, sentencesForWord.size())
        }
    }

    private void writeWordFrequencies() {
        def metadata = new File("$fileWithoutExtension/metadata.tsv")
        def entries = frequencyOfWords
                .sort { first, second -> second.value <=> first.value }
                .collect { k, v -> "$k\t$v" }

        metadata << entries.join("\n")
    }

    static void main(String[] args) {
        println "Available processors: " + Runtime.getRuntime().availableProcessors()

        if (args.size() != 1) {
            println "Give me the document to process."
            return
        }

        String fileName = args[0]
        DocumentProcessor documentProcessor = new DocumentProcessor(fileName)
        documentProcessor.process()
    }
}
